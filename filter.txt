! Title: Jaden's uBlock filters
! Expires: 1 week
! Description: Supplemental filters targeting uBlock origin
! Homepage: https://gitlab.com/YaBoiBurner/uBlock-filters
! Licence: https://gitlab.com/YaBoiBurner/uBlock-filters/blob/master/LICENSE

! -- UNIVERSAL FILTERS --
/amp-ads-
/amp-auto-ads-
/amp-facebook-
/amp-google-assistant-assistjs-
/amp-sticky-ad-
/amp-story-auto-ads-
/amp-story-auto-analytics-
/wp-content/plugins/abc-analytics/*
/wp-content/plugins/easy-affiliate-links/*
/wp-content/plugins/instagram-feed/*
/wp-content/plugins/socialsnap-pro/*
/wp-content/plugins/super-socializer/js/front/facebook/*$important
/x-kinja-static/assets/new-client/runtime~trackers.*$script,1p

||assets.readthedocs.org/static/javascript/readthedocs-analytics.js$script
||clickhole.com/api/videoupload/video/byIds$xhr
||i.kinja-img.com/gawker-media/image/upload/c_fill,f_auto,fl_progressive,g_center,h_264,q_80,w_470/*$image
||kinja.com/api/profile/accountwithtoken$script
||origin-images.wikia.com/fandom-ae-assets/tracking-opt-in/*$script
||static.parastorage.com/services/chat-worker/*
||x.kinja-static.com/assets/new-client/category-stream~second-scroll.*$script
||x.kinja-static.com/assets/new-client/customHeader~header.$script
||x.kinja-static.com/assets/new-client/header.$script
||x.kinja-static.com/assets/new-client/header~search.$script
||x.kinja-static.com/assets/new-client/header~second-scroll.$script
||x.kinja-static.com/assets/new-client/header~second-scroll~subscribe.$script
||x.kinja-static.com/assets/new-client/header~second-scroll~top-bar.*$script
||x.kinja-static.com/assets/new-client/initSubscribePopover.*$script
||x.kinja-static.com/assets/new-client/recent-video.*$script
||x.kinja-static.com/assets/new-client/second-scroll.*$script
||x.kinja-static.com/assets/new-client/second-scroll~special-sections-editor.*$script
||x.kinja-static.com/assets/new-client/subscribe.*$script
||x.kinja-static.com/assets/new-client/top-bar.$script

! -- STRICT BLOCKING OF ENTIRE SITES ---

! - Spam, scams, etc. -
! Try to make shre this doesn't have any redundancies with easylist
||betterhelp.com^
||donkeyfun.net^
||joy-penguin.com^
||lincolnproject.us^
||teechip.com^
||teechip.us^
||tw33tr.com^

! - Concerning content/management -
||alexgleason.me^$important
||americancoinstash.com^$important
||aogami.soup.io^$important
||artcamp.com^$important
||awooassociation.org^$important
||bbcgossip.com^$important
||benbankas.com^$important
||bizymouse.com^$important
||blenderguru.com^$important
||blick.ch^$important
||boundingintocomics.com^$important
||canine.equipment^$important
||celebritiesbutbald.com^$important
||chedot.com^$important
||commissionpaperwings.weebly.com^$important
||dailywire.com^$important
||disclose.tv^$important
||donthugcacti.com^$important
||dreamkeeperscomic.com^$important
||exitmouse.rocks^$important
||fangcon.com^$important
||fox10phoenix.com^$important
||furaffinity.net^$important
||furfest.org^$important
||furryartpro.com^$important
||gab.com^$important
||gamexplain.com^$important
||ganyiao.com^$important
||garyvarvel.com^$important
||gog.com^$important
||grrrgraphics.com^$important
||hanszimmer.com^$important
||hanszimmerlive.com^$important
||helleborusflos.carrd.co^$important
||helpsyria.carrd.co^$important
||iankelling.org^$important
||ib.metapix.net^$important
||inkbunny.net^$important
||jeffreytoobin.com^$important
||justfurus.weebly.com^$important
||kiwifarms.net^$important
||ko-fi.com/paperwings^$important
||linuxreviews.org^$important
||lookleft.us^$important
||lp.org^$important
||madefuryou.com^$important
||marapets.com^$important
||marzipansmilktea.wixsite.com^$important
||newscientist.com^$important
||nookazon.com^$important
||nypost.com^$important
||oneangrygamer.net^$important
||paperwings.store^$important
||parler.com^$important
||patreon.com/paperwings^$important
||peacewolf.carrd.co^$important
||peacewolfcreations.com^$important
||peppercoyote.bandcamp.com^$important
||peta.org^$important
||quillette.com^$important
||reference.pictures^$important
||repology.org^$important
||rodscontracts.com^$important
||sacanime.com^$important
||serioustransvibes.com^$important
||siamusic.net^$important
||syberwuff.com^$important
||syria.carrd.co^$important
||tailbait.pw^$important
||tailsandtornadoes.org^$important
||technicallyfox.com^$important
||teespring.com^$important
||thedonald.win^$important
||theguardian.com^$important
||theworldnews.net^$important
||thexenoforge.wixsite.com^$important
||tiny-conductor.tumblr.com^$important
||twintailcreations.com^$important
||upi.com^$important
||washingtontimes.com^$important
||weredog.co.uk^$important
||wikifur.com^$important
||wild---life.com^$important
||wildabandon.com^$important
||wolfjlupus.com^$important

! - Shitty news sites -
||bbc.co.uk^$1p
||bbc.com^$1p
||cnn.com^$1p
||dailymail.co.uk^$1p
||ft.com^$1p
||itsfoss.com^$1p
||lx.com^$1p
||nypost.com^$1p
||nytimes.com^$1p
||politico.com^$1p
||the-sun.com^$1p
||theguardian.co.uk^$1p
||theguardian.com^$1p
||thescottishsun.co.uk^$1p
||thesun.co.uk^$1p
||thesun.ie^$1p

! - Opinionated filters -
||3lau.com^$1p
||alexiscstudio.com^$1p
||battle.net^
||beeple-collect.com^$1p
||beeple-crap.com^$1p
||blizzard.com^
||boysnoize.com^$1p
||cal.new^
||deck.new^
||diablo3.com^
||doc.new^
||docs.new^
||document.new^
||documents.new^
||form.new^
||forms.new^
||gemini.com^$1p
||jam.new^
||jnsilva.com^$1p
||keep.new^
||killeracid.com^$1p
||meatcanyon.shop^$1p
||meet.new^
||meeting.new^
||new.meet^
||note.new^
||notes.new^
||odesza.com^$1p
||playhearthstone.com^
||playoverwatch.com^
||presentation.new^
||script.new^
||sheet.new^
||sheets.new^
||site.new^
||sites.new^
||slide.new^
||slides.new^
||slimesunday.com^$1p
||spaceyacht.net^$1p
||spreadsheet.new^
||spreadsheets.new^
||ssx3lau.art^$1p
||starcraft2.com^
||steveaoki.com^$1p
||thankyoux.com^$1p
||todayodious.com^$1p
||website.new^
||worldofwarcraft.com^

! - Disabled due to sites being useful as a reference -
! ||niftygateway.com^$1p
! ||superrare.co^$1p

! -- SITE-SPECIFIC FILTERS --

! apkmirror.com
apkmirror.com##.promotedApp

! arstechnica.com
arstechnica.com##^.ad
arstechnica.com##^#recommendations-footer
arstechnica.com##.xrail
||arstechnica.com/hotzones/src/ads.js$xhr,1p

! bandcamp.com
bandcamp.com###email-intake

! derpibooru.org
derpibooru.org##^#imagespns

! fandom.com, wikia.org
! There's a lot to filter here (including stuff that I haven't tested in a while)
fandom.com,wikia.org###WikiaBar
fandom.com,wikia.org###WikiaRailWrapper
fandom.com,wikia.org###WikiaTopAds
fandom.com,wikia.org###articleComments
fandom.com,wikia.org##.page-header__contribution
fandom.com,wikia.org##.wds-global-footer
fandom.com,wikia.org##.wds-global-navigation__links
fandom.com,wikia.org##.wds-global-navigation__start-a-wiki
fandom.com,wikia.org##.wds-global-navigation__user-menu
fandom.com,wikia.org##.wikia-photogallery-add
fandom.com,wikia.org##^script:has-text(function trackPageView)
fandom.com,wikia.org##^script:has-text(var ads)
fandom.com,wikia.org##^script:has-text(wikia_beacon_id)
||wikia.nocookie.net/spotlightsimagestemporary/*$image,important
||fandom.com/wikia.php?controller=ArticleCommentsController$xhr
||fandom.com/wikia.php?controller=RecirculationApi$xhr
||services.fandom.com/knowledge-graph/affiliates/*$xhr
||services.fandom.com/recommendations/*$xhr
@@||fandom.com/load.php?*&modules=AuthModal-*.js$script,1p
@@||fandom.com/load.php?*&modules=geo-*.js$script,1p

! furaffinity.net
furaffinity.net###donation_list

! furrylife.online
furrylife.online#@#.cShareLink

! gamebanana.com
gamebanana.com##.AdTagModule

! gamingonlinux.com
gamingonlinux.com##.btn-google.btn-auth

! genius.com
genius.com##apple-music-player
genius.com##recirculated-content

! housepetscomic.com
housepetscomic.com##.popmake
||housepetscomic.com/wp-content/plugins/popup-maker/*$script,1p

! humblebundle.com
humblebundle.com##.image-grid-view
humblebundle.com##.mosaic-section
humblebundle.com###site-xpromo-banner

! imgur.com
imgur.com##.BottomRecirc
imgur.com##.CommentsList
imgur.com##.Footer-wrapper
imgur.com##.Gallery-EngagementBar
imgur.com##.Gallery-Sidebar
imgur.com##.ProfileNavbar-loggedOut
imgur.com##.Searchbar
||api.imgur.com/comment/v1/comments$xhr,1p
||api.imgur.com/post/v1/posts$xhr,1p

! github.com
github.com##a[href^="git-client://clone?repo="]
github.com##a[href^="x-github-client://openRepo/"]

! independent.co.uk
independent.co.uk##.sticky-video

! itch.io
itch.io##.youtube_banner

! jackbox.tv
jackbox.tv##.slick
jackbox.tv##.slick-track

! limetorrents.info
limetorrents.info###rightbar div:has-text(Advertisements)

! merriam-webster.com

! metabattle.com
metabattle.com###blockerEnabled
metabattle.com##^script:has-text(getCookie('premiumMessage'))
||metabattle.com/ads.js$script,1p

! nexusmods.com
nexusmods.com##.premium-block
nexusmods.com##.rj-supporter-wrapper
nexusmods.com###rj-logo-social .rj-social-wrapper
nexusmods.com###rj-vortex

! npmjs.com
npmjs.com##div[data-promotion-spot="below-header"]

! pypi.org
pypi.org###sticky-notifications

! reddit.com
reddit.com##.premium-banner-outer
||www.redditstatic.com/gold/premium/premium-title.png$image,domain=reddit.com

! sofurry.com
sofurry.com##^#sf-ads

! steamdb.info
steamdb.info##.panel-social

! torrentz2.eu + mirrors
torrentz2.eu,torrentz2.is,torrentz.pl,torrentsmirror.com##.downurls > dl:has([href="/lorem2/xyy/"])
torrentz2.eu,torrentz2.is,torrentz.pl,torrentsmirror.com##.onToleratds

! twofactorauth.org
twofactorauth.org##.facebook-button

! uncensorpat.ch
uncensorpat.ch###ad-panel-bottom
uncensorpat.ch###ad-panel-top

! uquiz.com
uquiz.com##.show.trending_header
uquiz.com##.side_bar

! xda-developers.com
forum.xda-developers.com##.xdaforumnews.widget
forum.xda-developers.com##.xdanews.widget

! -- UNIMPORTANT/REDUNDANT FILTERS --
! This is for filters that are redundant with a list that uBlock provides, but
! leaves off by default.
! Anything redundant with a filter that is on by default should be removed.
! Additionally, anything redundant with a uBlock filter should be removed.
!
! - adguard-annoyance -
bleepingcomputer.com##.bc_news_letter_sidebar
kotaku.com###newsletterPromoModule
merriam-webster.com###recirc-bar-footer
myanimelist.net##.btn-mal-service
readthedocs.io##.keep-us-sustainable
urbandictionary.com##.mug-ad
userstyles.org##.android_button_banner
userstyles.org##.android_button_button
userstyles.org##.walking
youtube.com##.ytp-cards-teaser

! - fanboy-annoyance -
amazon.com###nav-upnav
bandcamp.com##.corpbanner
dropbox.com###top-notification-bar-container
eu.usatoday.com##.banner
kotaku.com##.js_commerce-inset-permalink
kotaku.com##.js_modal_exit_intent
theverge.com###newsletter-signup-short-form

! - adguard-annoyance & fanboy-annoyance -
dropbox.com###eu-cookie-bar
merriam-webster.com###subscribe-overlay
tapas.io###gdpr-cookie-message
userstyles.org##.NotificationLine
vox.com###newsletter-signup-short-form

! vim:ft=ublock
